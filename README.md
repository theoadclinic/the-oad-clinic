The OAD Clinic provides treatment for drug addiction and alcohol dependence in London. Bespoke outpatient, inpatient (rehab) and detox service. Our team of experts deliver medical interventions coupled with bespoke psychotherapy programmes to help you stop drinking and address drug related problems.

Address: 25A Eccleston Street, London SW1W 9NP, UK

Phone: +44 20 7823 6840

Website: https://www.theoadclinic.com
